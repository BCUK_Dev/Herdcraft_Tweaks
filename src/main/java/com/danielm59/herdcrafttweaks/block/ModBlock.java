package com.danielm59.herdcrafttweaks.block;

import com.danielm59.herdcrafttweaks.HerdcraftTweaks;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class ModBlock extends Block
{
	public ModBlock(String name)
	{
		super(Material.ROCK);
		setRegistryName(HerdcraftTweaks.MOD_ID, name);
		setUnlocalizedName(getRegistryName().toString());
	}
}
