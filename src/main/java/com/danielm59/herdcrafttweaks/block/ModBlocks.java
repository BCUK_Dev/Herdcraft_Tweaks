package com.danielm59.herdcrafttweaks.block;

import com.danielm59.herdcrafttweaks.HerdcraftTweaks;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Forge will automatically look up and bind block to the fields in this class
 * based on their registry name.
 */
@GameRegistry.ObjectHolder(HerdcraftTweaks.MOD_ID)
public class ModBlocks
{
	public static final ModBlock bcukBlock = null;
}

