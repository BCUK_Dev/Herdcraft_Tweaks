package com.danielm59.herdcrafttweaks.handler;

import com.danielm59.herdcrafttweaks.item.ModItems;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;

/**
 * Created by Daniel on 13/10/2017.
 */
public class RecipeHandler
{
	public static void init()
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(ModItems.bcukBlock,
				" B ", "WSW", " W ",
				'B', new ItemStack(Items.DYE, 1, 0),
				'W', new ItemStack(Items.DYE, 1, 15),
				'S', new ItemStack(Blocks.STONE)
		));

	}
}
