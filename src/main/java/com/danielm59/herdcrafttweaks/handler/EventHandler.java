package com.danielm59.herdcrafttweaks.handler;

import com.danielm59.herdcrafttweaks.block.ModBlock;
import com.danielm59.herdcrafttweaks.block.ModBlocks;
import com.danielm59.herdcrafttweaks.item.ModItemBlock;
import com.danielm59.herdcrafttweaks.item.ModItems;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * This is a special class that listens to registry events, to allow creation of mod block and item at the proper time.
 */
@Mod.EventBusSubscriber
public class EventHandler
{
	/**
	 * Listen for the register event for creating custom block
	 */
	@SubscribeEvent
	public static void addBlocks(RegistryEvent.Register<Block> event)
	{
		event.getRegistry().register(new ModBlock("bcukblock"));
	}

	/**
	 * Listen for the register event for creating custom item
	 */
	@SubscribeEvent
	public static void addItems(RegistryEvent.Register<Item> event)
	{

		event.getRegistry().register(new ModItemBlock(ModBlocks.bcukBlock));

	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event)
	{
		ModelLoader.setCustomModelResourceLocation(ModItems.bcukBlock,0, new ModelResourceLocation(ModItems.bcukBlock.getRegistryName(), "inventory"));
	}
}
