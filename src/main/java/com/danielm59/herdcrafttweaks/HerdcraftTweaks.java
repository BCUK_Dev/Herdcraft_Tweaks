package com.danielm59.herdcrafttweaks;

import com.danielm59.herdcrafttweaks.handler.RecipeHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(
		modid = HerdcraftTweaks.MOD_ID,
		name = HerdcraftTweaks.MOD_NAME,
		version = HerdcraftTweaks.VERSION
)
public class HerdcraftTweaks
{

	public static final String MOD_ID = "herdcrafttweaks";
	public static final String MOD_NAME = "Herdcraft Tweaks";
	public static final String VERSION = "@VERSION@";

	/**
	 * This is the instance of your mod as created by Forge. It will never be null.
	 */
	@Mod.Instance(MOD_ID)
	public static HerdcraftTweaks INSTANCE;

	/**
	 * This is the first initialization event. Register tile entities here.
	 * The BLock and Item registry events will have fired prior to entry to this method.
	 */
	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event)
	{
		RecipeHandler.init();
	}

	/**
	 * This is the second initialization event. Register custom recipes
	 */
	@Mod.EventHandler
	public void init(FMLInitializationEvent event)
	{

	}

	/**
	 * This is the final initialization event. Register actions from other mods here
	 */
	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event)
	{

	}
}
