package com.danielm59.herdcrafttweaks.item;

import com.danielm59.herdcrafttweaks.HerdcraftTweaks;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Forge will automatically look up and bind item to the fields in this class
 * based on their registry name.
 */
@GameRegistry.ObjectHolder(HerdcraftTweaks.MOD_ID)
public class ModItems
{
	public static final ItemBlock bcukBlock = null;
}
