package com.danielm59.herdcrafttweaks.item;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;

/**
 * Created by Daniel on 13/10/2017.
 */
public class ModItemBlock extends ItemBlock
{
	public ModItemBlock(Block block)
	{
		super(block);
		setRegistryName(block.getRegistryName());
		setUnlocalizedName(getRegistryName().toString());
	}
}
